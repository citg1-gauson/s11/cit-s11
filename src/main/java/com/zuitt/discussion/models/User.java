package com.zuitt.discussion.models;
import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    // properties
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String username;
    @Column
    private String password;

    // constructor
    public User(){}
    public User(String username,String password){
        this.username = username;
        this.password = password;
    }

    //getters and setters
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
}
